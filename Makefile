CC=gcc
CFLAGS=-Wall -Werror 
FICHIERO= linked_lists.o fms.o main.o primitives.o Shell.o
LIBS=

default: clean linked_lists fms primitives Shell main compile
	rm -f *.o
	./GFC

compile:
	$(CC) $(CFLAGS) -lm $(FICHIERO) -o GFC $(LIBS)

%: %.c
	$(CC) $(CFLAGS) -c $@.c -o $@.o

clean:
	clear
	rm -f *.o
	rm -f exemple
	rm -f *.tar
	rm -f GFC
