#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "Shell.h"



//Commands list

char *cmd_List[] = {
  "cp", //copy (file/directory)
  "ls",
  "mkdir",
  "link",
  "unlink",
  "rm",
  "touch",
  "cat",
  "rmdir",
  "echo",//’echo ”texte” > file’
  "exit", //Exit the shell
  "clear",//Clear the terminal screen
  "help"
};

  //************ Interprete la commande prise dans le buffer et renvoie le nombre de parametre ********
int InterpreteCommande(char *cmd, char ***command_traite){
	char tb[10][50];
	int i,nbr;
	nbr = 0;
	int taille_tb=0;
	for(i=0; i<strlen(cmd); i++)  //recharge les differents parametres de la commande dans un tableau a deux dimensions
	{
		if(cmd[i]==' ' || cmd[i]=='\n')   //parcourt la commande a la recherche des espaces et du caractere retour a la ligne
		{
			tb[nbr][i-taille_tb]='\0';
			taille_tb += strlen(tb[nbr]) + 1;
			nbr++;
			}else{
				tb[nbr][i-taille_tb]=cmd[i];
				}
		}
		*command_traite = (char**)realloc(*command_traite, sizeof(char*)*(nbr+1));
		for(i=0; i<nbr; i++)
		{
			(*command_traite)[i] = (char*)malloc(sizeof(char)*strlen(tb[i]));
			strcpy((*command_traite)[i], tb[i]);
			}
		return nbr;
}

  //************ recherche et affiche la liste des noms des fichiers et dossiers contenu dans le repertoire courant ********
void command_ls(DISK *disk){
	int j;
	for(j=0; j<NBINODE; j++){
		if(disk->inode[j].typeFile != 0){
			if(disk->inode[j].typeFile == 1){
				printf("\033[0;36m%s\033[0m ", disk->inode[j].name);
			}else
			if(disk->inode[j].typeFile == 2){
				printf("\033[0;37m%s\033[0m ", disk->inode[j].name);
			}else
			if(disk->inode[j].typeFile == 3){
				printf("\e[35m%s ", disk->inode[j].name);
			}else{
				printf("%s ", disk->inode[j].name);
			}

		}
	}
		printf("\n");
}
/* create file(s) */
void command_touch(DISK *disk, char **cmd, int nbr){
	int i,j,etat;
	etat = 0;
	for(i=0; i<nbr-1; i++){
		for(j=0; j<NBINODE; j++){  // research free inodes and creation of file in the first founded
			if (disk->inode[j].typeFile == 0){
				disk->inode[j] = mycreat(cmd[i+1]);
				etat = 1;
				break;
			}
		}
		if(etat == 0){  // if inode are not found
			printf("impossible to create the file %s \n", cmd[i+1]);
			}
		etat = 0;
		}
	save_FMS(*disk);

}


/* Copy file(s) */
void command_cp(DISK *disk, char **cmd, int nbr){
	int j,etat;
	etat = 0;
	char *buff;

		for(j=0; j<NBINODE; j++){  // research free inodes and creation of copy file in the first founded
			if (disk->inode[j].typeFile == 0){
				if(strcmp(cmd[1],cmd[2])==0){
					perror("The files already exists, please change the file name");
				}else{
					disk->inode[j]=myopen(*disk, cmd[2], "rwx");//Create and open the copy file
					buff=myread(*disk, myopen(*disk, cmd[1], "rwx"));//Read the file content
					mywrite(disk, &disk->inode[j], buff);//write the content into the copy
					myclose(&disk->inode[j]);//close the copy file
					etat = 1;
					break;
				}

			}
		}
		if(etat == 0){  // if inode are not found
			printf("impossible to copy the file %s \n", cmd[1]);
		}
		etat = 0;

	save_FMS(*disk);

}
//Clear terminal screen
void command_clear(){
	printf("\e[2J\e[H");
}

/* print content of a file to the sreen */
void command_cat(DISK *disk, char **cmd, int nbr){
	int i,j,etat;
	etat = 0;
	for(i=0; i<nbr-1; i++){
		for(j=0; j<NBINODE; j++){
			if (disk->inode[j].typeFile == 1 && (strcmp(cmd[i+1], disk->inode[j].name) == 0)){
				disk->inode[j]=myopen(*disk, cmd[i+1], "rx");
				printf(" \n%s \n\n", myread(*disk, disk->inode[j]));
				free(myread(*disk, disk->inode[j]));
				myclose(&disk->inode[j]);
				etat = 1;
				//break;
			}
		}
		if(etat == 0){
		printf("file %s dosen't exist. \n", cmd[i+1]);
			}
		etat = 0;
	}
}

/* delete file(s) */
void command_rm(DISK *disk, char **cmd, int nbr){
	int i,j,etat;
	etat = 0;
	for(i=0; i<nbr-1; i++){
		for(j=0; j<NBINODE; j++){
			if ((disk->inode[j].typeFile == 1 || disk->inode[j].typeFile == 3 ) && (strcmp(cmd[i+1], disk->inode[j].name) == 0)){
				myrm(disk, disk->inode[j].name);
				printf("file  %s   delete \n", cmd[i+1]);
				etat = 1;
				//break;
			}
		}
		if(etat == 0){
		printf("file %s doesn't exist. \n", cmd[i+1]);
			}
		etat = 0;
		}
	save_FMS(*disk);
}

/* write/(create and write) a content in a file */
void command_echo(DISK *disk, char **cmd, int nbr){
	int j,etat;
	etat = 0;
	if(nbr >= 4){
		if(strcmp(cmd[2], ">") == 0){
			for(j=0; j<NBINODE; j++){
				if (disk->inode[j].typeFile == 1 && (strcmp(cmd[3], disk->inode[j].name) == 0)){
					printf("founded %s\n", cmd[1]);
					disk->inode[j] = myopen(*disk, cmd[3], "w");
					mywrite(disk, &disk->inode[j], cmd[1]);
					myclose(&disk->inode[j]);
					etat = 1;
					break;
				}
			}
			if(etat == 0){
				for(j=0; j<NBINODE; j++){
					if (disk->inode[j].typeFile == 0){
						disk->inode[j] = myopen(*disk, cmd[3], "w");
						mywrite(disk, &disk->inode[j], cmd[1]);
						myclose(&disk->inode[j]);
						etat = 1;
						break;
					}
				}
			}
			else if(etat == 0){  // if inode are not found
				printf("impossible to create the file %s \n", cmd[3]);
			}
			etat = 0;
			save_FMS(*disk);
		}else{
		printf("Command untraceable or incomplete \n");
		}
	}else{
		printf("Command untraceable or incomplete \n");
	}
}

/* Link file(s) */
void command_link(DISK *disk, char **cmd){
	int j,etat;
	etat = 0;
	for(j=0; j<NBINODE; j++){  // research free inodes and creation of link file in the first founded
		//if the file exits and the file(link) does not exist then create a link
		if (disk->inode[j].typeFile == 1 && (strcmp(cmd[1], disk->inode[j].name) == 0 && !strcmp(cmd[2], disk->inode[j].name) == 0)){
				mylink(disk, cmd[1], cmd[2]);	//create a link between the files
				printf("The link is created between %s and %s\n",cmd[1],cmd[2]);
				etat=1;
				break;
			}
	}
	if(etat == 0){  // if inode are not found
		printf("impossible to link the files \n");
	}
	save_FMS(*disk);
}


/* unlink the files */
void command_unlink(DISK *disk, char **cmd){
	int j,etat;
	etat = 0;
	for(j=0; j<NBINODE; j++){
		if (disk->inode[j].typeFile == 0){// research if the link file does exists
			 myunlink(disk, cmd[1]);	//delete the link between the files
			printf("The link is destroyed succefully");
			break;
		}
	}
	if(etat == 0){  // if inode are not found
		printf("impossible to unlink the files \n");
	}
	etat = 0;
	save_FMS(*disk);
}
/*folder command*/
void command_mkdir(DISK *disk,int inodefather,char **cmd, int nbr){
	int i,j,etat;
	etat = 0;
	for(i=0; i<nbr-1; i++){
		for(j=0; j<NBINODE; j++){  // research free inodes and creation of folder in the first founded
			if (disk->inode[j].typeFile == 0){
				INODE tmp = mymkdir(disk,cmd[i+1],inodefather,j);
				if(tmp.typeFile != 0){
					disk->inode[j] = tmp;
				}
				else {
					printf("Folder not created\n");
				}

				etat = 1;
				break;
			}
		}
		if(etat == 0){  // if inode are not found
			printf("impossible to create the folder%s \n", cmd[i+1]);
			}
		etat = 0;
		}
	save_FMS(*disk);
}
void command_rmdir(DISK *disk, char **cmd, int nbr){
	int i,j,state;
	state = 0;
	for(i=0; i<nbr-1; i++){
		for(j=0; j<NBINODE; j++){
			if (disk->inode[j].typeFile == 2 && (strcmp(cmd[i+1], disk->inode[j].name) == 0)){
				myrmdir(disk, disk->inode[j].name);
				printf("folder %s   delete \n", cmd[i+1]);
				state = 1;
				//break;
			}
		}
		if(state == 0){
		printf("folder %s doesn't exist !!! \n", cmd[i+1]);
			}
		state = 0;
		}
	save_FMS(*disk);
}
//help command
void command_help(char **cmd,int nbr){
    int i=0;
    int nbCommands=(int)sizeof(cmd_List) / sizeof(char *);
    for (i = 0; i <nbr-1; i++) {
            if(strcmp(cmd[i+1],"ls") == 0){
                printf("ls : List files and directory\n");
            }
            else if(strcmp(cmd[i+1],"cp") == 0){
                    printf("cp : Command makes copies of files and directories.\n");
                    printf("Syntax : cp \e[3msource\e[0m  \e[3mdestination\e[0m  \n");
            }
            else if(strcmp(cmd[i+1],"mkdir") == 0){
                    printf("mkdir : Command makes create directories.\n");
                    printf("Syntax : mkdir \e[3mdirectory\e[0m \n");
            }
            else if(strcmp(cmd[i+1],"touch") == 0){
                    printf("rm : Command makes create files\n");
                    printf("Syntax : touch \e[3mfile\e[0m \n");
            }
            else if(strcmp(cmd[i+1],"rmdir") == 0){
                    printf("rmdir : Command removes empty directories from a filesystem\n");
                    printf("Syntax : rmdir \e[3mdirectory\e[0m \n");
            }
            else if(strcmp(cmd[i+1],"rm") == 0){
                    printf("rm : Command removes files from a filesystem\n");
                    printf("Syntax : rm \e[3mfile\e[0m \n");
            }
            else if(strcmp(cmd[i+1],"cat") == 0){
                    printf("cat : Command reads data from files, and outputs their contents.\n");
                    printf("Syntax : cat \e[3mfile\e[0m \n");
            }
            else if(strcmp(cmd[i+1],"echo") == 0){
                    printf("echo : The echo command prints text to standard output.\n");
                    printf("Syntax : echo \e[3mtexte\e[0m > \e[3mfile\e[0m\n");
            }
            else if(strcmp(cmd[i+1],"exit") == 0){
                    printf("exit : Command causes the shell to exit.\n");
                    printf("Syntax : exit \n");
            }
            else if(strcmp(cmd[i+1],"clear") == 0){
                    printf("clear : The clear command clears the screen.\n");
                    printf("Syntax : clear \n");
            }
            else if(strcmp(cmd[i+1],"link") == 0){
                    printf("link : Command associates a file with a file name in a file system.\n");
                    printf("Syntax : link \e[3mfile1\e[0m \e[3mfile2\e[0m\n");
            }
            else if(strcmp(cmd[i+1],"unlink") == 0){
                    printf("unlink : Command calls and directly interfaces with the unlink system function,which removes a specified file. \n");
                    printf("Syntax : unlink \e[3mfile\e[0m \n");
            }


    }
    if(nbr == 1){
            printf("These shell commands are internally defined. Enter \"help\" to see this list.\n Type \"help name\" to find out more about the function called \"name\".\n");
            for(i=0;i<nbCommands;i++){
                printf("%s\n",cmd_List[i]);
            }
    }

}
// Greeting shell screen
void init_shell(DISK *disk){
	printf("\e[2J\e[H");  //Clear the screen
	printf("\n\t\t#############################################");
	 printf(R"EOF(
		| |                    (_)           | |
		| |_ ___ _ __ _ __ ___  _ _ __   __ _| |
		| __/ _ \ '__| '_ ` _ \| | '_ \ / _` | |
		| ||  __/ |  | | | | | | | | | | (_| | |
		 \__\___|_|  |_| |_| |_|_|_| |_|\__,_|_|
	)EOF");

	printf("\n\n\n\t\t\t##########MYSHELL##########");
	printf("\n\n\t\t    SYSTEM PROJECT | ISTY 2019 IATIC3 ");
	printf("\n\t\t#############################################\n");
	printf("\n\t Demarage de l'interpreteur.........\n");
	sleep(2); //Wait for 2 seconds
	printf("\e[2J\e[H");
	char buffer[CMD_BUFFER_SIZE];	// buffer to hold command line
	char **commande = (char**)malloc(sizeof(char*));

	//Show greeting shell screen
   	while(1){
    	/*Prompt and read the input*/
    	printf("\033[0;31m[myshell] $\033[0m ");
		fflush(stdout);
		fgets(buffer,CMD_BUFFER_SIZE,stdin);
		int nbre_mot = InterpreteCommande(buffer, &commande);
		execute_command(disk, commande, nbre_mot);
	}
}

/* execute command */
int execute_command(DISK *disk, char **cmd, int nbr)
{
	int nbCommands=(int)sizeof(cmd_List) / sizeof(char *);
	int next=0;

	for (int i = 0; i < nbCommands; i++) {
		if (strcmp(cmd[0], cmd_List[i]) == 0) {
			next = i + 1;
			break;
		}
	}

	switch(next) {
		case 1:
			command_cp(disk, cmd,nbr);
			return 1;
		case 2:
			command_ls(disk);
			return 1;
		case 3:
			command_mkdir(disk,0,cmd,nbr); //0 : inodefather to be change
			return 1;
		case 4:
			command_link(disk,cmd);
			return 1;
		case 5:
			command_unlink(disk,cmd);
			return 1;
		case 6:
			command_rm(disk,cmd,nbr);
			return 1;
		case 7:
			command_touch(disk,cmd,nbr);
			return 1;
		case 8:
			command_cat(disk,cmd,nbr);
			return 1;
		case 9:
			command_rmdir(disk,cmd,nbr);
			return 1;
		case 10:
			command_echo(disk,cmd,nbr);
			return 1;
		case 11:
			exit(0);
			return 0;
		case 12:
			command_clear();
			return 1;
        case 13:
    		command_help(cmd,nbr);
    		return 1;
		default:
			break;
	}
	return 0;
}
