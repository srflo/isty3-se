#ifndef SHELL_H
#define SHELL_H

#include "fms.h"
#include "primitives.h"
#include "linked_lists.h"

#define CMD_BUFFER_SIZE 100
#define CMD_NARGS 1024
#define CMD_DELIM " \t\n\r\a"


void init_shell(DISK *disk);
int execute_command(DISK *disk, char **cmd, int nbr);
int InterpreteCommande(char *cmd, char ***command_traite);
void command_ls(DISK *disk);
void command_touch(DISK *disk, char **cmd, int nbr);
void command_cat(DISK *disk, char **cmd, int nbr);
void command_rm(DISK *disk, char **cmd, int nbr);
void command_echo(DISK *disk, char **cmd, int nbr);
void command_cp(DISK *disk, char **cmd,int nbr);
void command_link(DISK *disk, char **cmd);
void command_unlink(DISK *disk, char **cmd);
void command_mkdir(DISK *disk,int inodefather,char **cmd, int nbr);
void command_rmdir(DISK *disk, char **cmd, int nbr);
#endif
