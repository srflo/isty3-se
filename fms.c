#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include "fms.h"

void create_FMS(DISK* disk){
	int i,j;
	for(i=0;i<NBINODE;i++){
		for(j=0;j<3;j++){
			disk->inode[i].permission[j]='-';
		}
		disk->inode[i].typeFile=0;
		disk->inode[i].lenghtFile=0;
		disk->inode[i].blockNumber = init_list();
		disk->inode[i].state = 0;
		
		disk->inode[i].name[0] = '0';
		disk->inode[i].name[1] = '\0';
	}
	for(i=0;i<NBBLOCK;i++){
		
		disk->blockUse[i]=-1;
		
		for(j=0;j<13;j++){
			disk->inode[i].modificationDate[j]=0;	
		}
		
		for(j=0;j<=SIZE_BLOCK;j++){
			disk->block[i].data[j]='0';
		}
		disk->block[i].data[SIZE_BLOCK-1]='\0';
	}
	disk->blockUse[0]=-1;

}

void save_FMS(DISK disk){
	
	int i,j,w;
	
	FILE * file=fopen(FICHIER_SAVE,"w");
	
	printf("save start\n");
	
	fprintf(file,"BlockUse\n");	
	for(i=0;i<NBBLOCK;i++){
		fprintf(file,"%d_",disk.blockUse[i]);	
	}
	fprintf(file,"\n");	
	
	fprintf(file,"Block\n");	
	for(i=0;i<NBBLOCK;i++){
		fprintf(file,"%2d: %s\n",i,disk.block[i].data);	
	}
	
	
	for(i=0;i<NBINODE;i++){
		
		fprintf(file,"Inode\n");
		fprintf(file,"%d\n",i);
		for(w=0;w<3;w++){
			fprintf(file,"%c",disk.inode[i].permission[w]);
		}	
		fprintf(file,"\n");
		
		fprintf(file,"%d\n",disk.inode[i].typeFile);	
		
		//List start
		List *l = disk.inode[i].blockNumber;
		fprintf(file,"s_%d\n",size_list(l));	
		
		
		if(l){
			origin_node(l);
			while(l->currentNode != NULL){
				fprintf(file,"%d\n",value_node(l));	
				printf("%d\n",value_node(l));
				next_node(l);
			}
		}
		//List end
		//printf("liste end\n");
		
		fprintf(file,"%d\n",disk.inode[i].lenghtFile);	
		
		for(j=0;j<13;j++){
			fprintf(file,"%d_",disk.inode[i].modificationDate[j]);	
		}
		fprintf(file,"\n");	
		
		//printf("date end \n");
		
		fprintf(file,"~%s\n",disk.inode[i].name);	
		fprintf(file,"%d\n",disk.inode[i].state);	
		
		fprintf(file,"\n");	
	}
	fclose(file);
	printf("save end\n");
}


void load_FMS(DISK * disk){

	int i,j,temp;
	char lecture[20];
	
	FILE *file=fopen(FICHIER_SAVE,"r");
	
	printf("load start\n");
	
	if(file==NULL){
		printf("Unable to read the file\nCreating an empty disk\n");
		create_FMS(disk);
	}
	else{
		while(fgets(lecture,20,file) !=NULL){	
			if(strcmp(lecture,"BlockUse\n")==0){
				for(i=0;i<NBBLOCK;i++){
					fscanf(file,"%d_",&(disk->blockUse[i]));
				}
			}
			if(strcmp(lecture,"Block\n")==0){
				for(i=0;i<NBBLOCK;i++){
					fscanf(file,"%d:%s",&temp,disk->block[i].data);
				}
			}
			if(strcmp(lecture,"Inode\n")==0){
				//for(i=0;i<NBINODE;i++){
				fscanf(file,"%d\n",&i);	
				
				fscanf(file,"%s\n",disk->inode[i].permission);	
						
				fscanf(file,"%d\n",&(disk->inode[i].typeFile));	
						
					//List start
					//printf("list start\n");
					
				List *l = init_list();
				int max,val;
				fscanf(file,"s_%d\n",&max);	

				origin_node(l);
				for(j=0;j<max;j++){
					fscanf(file,"%d\n",&val);	
					insert_node(l,val);
				}
				disk->inode[i].blockNumber=l;
				//printf("list end\n");
				//List end	
						
				fscanf(file,"%d\n",&(disk->inode[i].lenghtFile));	
						
				for(j=0;j<13;j++){
					fscanf(file,"%d_",&(disk->inode[i].modificationDate[j]));	
				}
				fscanf(file,"\n");	
				char name2[20];
				fscanf(file,"~%s\n",name2);	
				printf("name: ~%s~\n",name2);
				strcpy(disk->inode[i].name,name2);
					
				printf("sate\n");
				fscanf(file,"%d\n",&(disk->inode[i].state));	
					
				printf("inode end\n");
			}
			
		}
		fclose(file);
		printf("load end\n");
	}
}

void print_FMS(DISK disk){
	
	int i,j,w;
	
	printf("BlockUse\n");	
	for(i=0;i<NBBLOCK;i++){
		printf("%d",disk.blockUse[i]);	
	}
	printf("\n\n");	
	
	printf("Block\n");	
	for(i=0;i<NBBLOCK;i++){
		printf("%2d: %s\n",i,disk.block[i].data);	
	}
	printf("\n");	
	

	for(i=0;i<NBINODE;i++){
		
		printf("\nInode %d\n",i);
		printf("Permission:");
		for(w=0;w<3;w++){
			printf("%c",disk.inode[i].permission[w]);
		}	
		printf("\n");	
		
		printf("type file: %d\n",disk.inode[i].typeFile);	
		
		//List start
		printf("list start\n");
		List *l = disk.inode[i].blockNumber;
		printf("s_%d\n",size_list(l));	
		
		origin_node(l);
		for(j=0;j<size_list(disk.inode[i].blockNumber);j++){
			printf("%d\n",value_node(l));	
			next_node(l);
		}
		printf("list end\n");
		//List end
		
		
		printf("length file: %d\n",disk.inode[i].lenghtFile);	
		
		printf("date modif: ");
		for(j=0;j<13;j++){
			printf("%d_",disk.inode[i].modificationDate[j]);	
		}
		printf("\n");	
		printf("name: %s\n",disk.inode[i].name);	
		printf("state: %d\n",disk.inode[i].state);	
		
		printf("\n\n");	
	}
}
