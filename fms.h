#ifndef SGF_H
#define SGF_H
#include "linked_lists.h"
#define NBINODE 30
#define NBBLOCK 30
#define SIZE_BLOCK 1024

#define FICHIER_SAVE "save.txt"

typedef struct{
	char permission[3];
	int typeFile; // 0 -> isn't a file, 1 -> file, 2 -> repository
	List *blockNumber;
	int lenghtFile;
	int modificationDate[14];
	char name[20];
	int state; // 0 -> file close, 1 -> file open
}INODE;

typedef struct{
	char data[SIZE_BLOCK];
}BLOCK;

typedef struct{
	INODE inode[NBINODE];
	BLOCK block[NBBLOCK];
	int blockUse[NBBLOCK]; // -1 -> block free, 1 -> block used
}DISK;

void create_FMS(DISK* disk);
void save_FMS(DISK disk);
void load_FMS(DISK * disk);
void print_FMS(DISK disk);

#endif
