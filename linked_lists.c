#include <stdio.h>
#include <stdlib.h>
#include "linked_lists.h"


//Linked lists initialisation
List *init_list(){

    List *list = malloc(sizeof(*list));
    Node *node = malloc(sizeof(*node));
    
    if (node == NULL || list == NULL){
        exit(EXIT_FAILURE);
    }
    
    node->valueNode = -1;
    node->nextNode = NULL;
	list->firstNode = node;
    list->currentNode = NULL;

    return list;
}

//Insert node after the current one
void insert_node(List *list, int value){
	
    if(list){
		Node* currentNode = list->currentNode;
		Node* newNode = NULL;
		newNode = malloc(sizeof(*newNode));
		
		if(newNode){
			newNode->valueNode = value;
			
			if(currentNode == NULL){
				list->firstNode->nextNode = newNode;
				newNode->nextNode = NULL;
			}
			
			else{
				newNode->nextNode = currentNode->nextNode;
				currentNode->nextNode = newNode;
			}
			
			list->currentNode = newNode;
		}
	}
}

//Delete the node after the current one
void removeNext_node(List *list){
	
	if(list && list->currentNode && list->currentNode->nextNode){
		Node* currentNode = list->currentNode;
		Node* nodeSupp = NULL;
		nodeSupp = currentNode->nextNode;
		currentNode->nextNode = nodeSupp->nextNode;
		free(nodeSupp);
		nodeSupp = NULL;
	}
}

//Delete first node
void removeFirst_node(List *list){
	
	if(list){
		list->currentNode = list->firstNode;
		removeNext_node(list);
	}
}

//Next node becomes the current node
void next_node(List *list){
	
	if(list && list->currentNode)
		list->currentNode = list->currentNode->nextNode;
}

//Return value of the current node
int value_node(List *list){
	
	if(list && list->currentNode){
		return list->currentNode->valueNode;
	}
	
	else
		return -1;
}

//First node becomes the current node
void origin_node(List *list){
	
	if(list){
		list->currentNode = list->firstNode->nextNode;
	}
}

//Last node becomes the current node
void last_node(List *list){
	
	if(list){
		while(list->currentNode->nextNode != NULL){
			next_node(list);
		}
	}
}

//Size of a list
int size_list(List *list){
	
	int size = 0;
	
	if(list){
		origin_node(list);
		while(list->currentNode != NULL){
			size++;
			next_node(list);
		}
	}
	
	return size;
}

//Delete a list
void delete_list(List **plist){
	
	if(plist && *plist){
		origin_node(*plist);
		while((*plist)->currentNode->nextNode != NULL){
			removeNext_node(*plist);
		}
		
		removeFirst_node(*plist);
		free((*plist)->currentNode);
		(*plist)->currentNode = NULL;
		free(*plist);
		*plist = NULL;
	}
}

//Print a list (usefull for tests)
void print_list(List *list){
	
	if(list){
		origin_node(list);
		while(list->currentNode != NULL){
			printf("%d -> ",value_node(list));
			next_node(list);
		}
		
		printf("NULL\n");
	}
}





