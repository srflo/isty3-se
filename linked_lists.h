#ifndef LINKED_LISTS_H
#define LINKED_LISTS_H

//Structure of a linked list node
typedef struct Node Node;
struct Node{
	int valueNode;//node value
	Node *nextNode;//points to the next member of the list
};

//Structure of a linked list
typedef struct List List;
struct List{
	Node *firstNode;//points to the first node
	Node *currentNode;//points to the current node
};

List *init_list();
void insert_node(List *list, int value);
void removeNext_node(List *list);
void removeFirst_node(List *list);
void next_node(List *list);
int value_node(List *list);
void origin_node(List *list);
void last_node(List *list);
int size_list(List *list);
void delete_list(List **list);
void print_list(List *list);

#endif
