#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <math.h>
#include "linked_lists.h"
#include "fms.h"

/* Create a file
 * Return inode
 */
INODE mycreat(char name[20]){

	INODE inode;
	int i;

	// Initialisation
	inode.typeFile = 1;
	inode.lenghtFile = 0;
	inode.blockNumber = init_list();
	for(i=0;i<20;i++){
		inode.name[i]=name[i];
	}

	//inode.name[19]='\0';
	for(i=0;i<13;i++){
		inode.modificationDate[i]=0;
	}
	for(i=0;i<3;i++){
		inode.permission[i] = '-';
	}
	inode.permission[3] = '\0';

	inode.state = 1;

	return inode;
}

/* Open (and create if the file doesn't exists) a file
 * Return inode
 */
INODE myopen(DISK disk, char *name, char *mode){

	INODE inode;
	int i,j;


	// Search if the file already exists
	for(i=0;i<NBINODE;i++){
		if(strcmp(name, disk.inode[i].name) == 0){

			// Open the file
			disk.inode[i].state = 1;
			for(j=0;j<3;j++){
				disk.inode[i].permission[j] = '-';
			}
			inode.permission[3] = '\0';
			j=0;
			while((mode[j] == 'r')||(mode[j] == 'w')||(mode[j] == 'x')){
				if(mode[j] == 'r'){
					disk.inode[i].permission[0] = 'r';
				}
				else if(mode[j] == 'w'){
					disk.inode[i].permission[1] = 'w';
				}
				else if(mode[j] == 'x'){
					disk.inode[i].permission[2] = 'x';
				}
				j++;
			}

			// Return inode
			disk.inode[i].state = 1;
			return disk.inode[i];
		}
	}

	// Create new file
	inode = mycreat(name);

	//Open the new file
	j=0;
	while((mode[j] == 'r')||(mode[j] == 'w')||(mode[j] == 'x')){
		if(mode[j] == 'r'){
			inode.permission[0] = 'r';
		}
		else if(mode[j] == 'w'){
			inode.permission[1] = 'w';
		}
		else if(mode[j] == 'x'){
			inode.permission[2] = 'x';
		}
		j++;
	}
	inode.state = 1;

	// Return a new inode
	return inode;
}

// Close a file
void myclose(INODE *inode){

	inode->state = 0;
}

/* Read a file
 * Return char*
 */
char* myread(DISK disk, INODE inode){

	char *buffer;
	int i,j,k = 0,nBlock;

	origin_node(inode.blockNumber);

	// Verifications
	if((inode.permission[0] == 'r') && (inode.state == 1)){

		if(inode.lenghtFile == 0){
			printf("Empty file.\n");
			return "";
		}

		buffer = malloc(sizeof(char)*SIZE_BLOCK*inode.lenghtFile);

		for(i=0;i<inode.lenghtFile+1;i++){
			nBlock = value_node(inode.blockNumber);
		//	printf("buffer in read1 : %d\n k = %d",buffer[k],k );
			for(j=0;j<SIZE_BLOCK;j++){
				// End of the file
				if(disk.block[nBlock].data[j] == '\0'){
					buffer[k] = '\0';
					return buffer;
				}

				// Read
				buffer[k] = disk.block[nBlock].data[j];
				k++;


			}

			next_node(inode.blockNumber);
		}
	}

	else{
		printf("You don't have the right to read the file, or the file isn't open.\n");
	}

	return buffer;
}

/* Write in a file
 * Manages the allocation of new blocks
 */
void mywrite(DISK *disk, INODE *inode, char *buffer){

	int i = 0,k = -1,nBlock;

	// Verifications
	if((inode->permission[1] == 'w' && inode->state == 1)){
		nBlock = value_node(inode->blockNumber);

		if(inode->lenghtFile != 0){
			last_node(inode->blockNumber);
			while((disk->block[nBlock].data[i] != '\0')&&(i < SIZE_BLOCK)){
				i++;
			}
		}

		// While buffer isn't empty
		do{
			k++;

			// Allocate a new block
			if((i == SIZE_BLOCK)||(inode->lenghtFile == 0)){
				i=0;
				nBlock = 0;
				while((disk->blockUse[nBlock] != -1)&&(nBlock < NBBLOCK)){
					nBlock++;
				}
				if(nBlock == NBBLOCK){
					printf("Not enough memory.\n");
					return;
				}
		//		printf("size = %d\n",inode->lenghtFile);
		//		printf("buffer = %d\n",buffer[k]);
				inode->lenghtFile++;
				disk->blockUse[nBlock] = 1;
				insert_node(inode->blockNumber,nBlock);
			}

			// Write
			if((buffer[k] > 0) && (buffer[k] < 128)){
				disk->block[nBlock].data[i] = buffer[k];
				i++;
			}
			else{
				disk->block[nBlock].data[i] = '\0';
			}
		//	printf("buffer in mywrite %d\n",buffer[k]);
		}while((buffer[k] > 0) && (buffer[k] < 128));
	}

	else{
		printf("You don't have the right to write in the file, or the file isn't open.\n");
	}
}

// Delete a file
void myrm(DISK *disk, char *name){

	int i,j,k = 0,sizeList,nBlock;

	// Search if the file exists
	for(i=0;i<NBINODE;i++){
		if(strcmp(name,disk->inode[i].name)==0){
			k = 1;
			sizeList = size_list(disk->inode[i].blockNumber);
			origin_node(disk->inode[i].blockNumber);

			// Initialisation block used
			for(j=0;j<sizeList;j++){
				nBlock = value_node(disk->inode[i].blockNumber);
				disk->blockUse[nBlock]=-1;
				for(j=0;j<=SIZE_BLOCK;j++){
					disk->block[i].data[j]='0';
				}
				disk->block[i].data[SIZE_BLOCK-1]='\0';
				next_node(disk->inode[i].blockNumber);
			}

			// Initialisation if needed
			if(sizeList != 0){
				delete_list(&disk->inode[i].blockNumber);
				disk->inode[i].blockNumber = init_list();
			}
			for(j=0;j<13;j++){
				disk->inode[i].modificationDate[i]=0;
			}

			// Initialision inode
			for(j=0;j<3;j++){
				disk->inode[i].permission[j]='-';
			}
			disk->inode[i].typeFile=0;
			disk->inode[i].lenghtFile=0;
			disk->inode[i].state = 0;
			disk->inode[i].name[19] = '\0';
		}
	}

	// If the file doesn't exists
	if(k == 0){
			printf("The file doesn't exists.\n");
	}
}

void mylink(DISK *disk, char *name, char *name2){
	int i,idepart=-1,iarrive=-1;

	// Search if the file exists
	for(i=0;i<NBINODE;i++){
		if(strcmp(name,disk->inode[i].name)==0){
			idepart=i;
		}
		if(disk->inode[i].typeFile==0){
			iarrive=i;
		}
	}
	// If the file doesn't exists
	if(idepart==-1){
		printf("The file doesn't exists.\n");
	}
	else if(iarrive==-1){
		printf("There is no available inode.\n");
	}
	else{
		disk->inode[iarrive]=mycreat(name2);
		disk->inode[iarrive].blockNumber=disk->inode[idepart].blockNumber;
		disk->inode[iarrive].typeFile=3;
	}
}

void myunlink(DISK *disk, char *name){
	int i,trouve=-1;
	// Search if the file exists
	for(i=0;i<NBINODE;i++){
		if(strcmp(name,disk->inode[i].name)==0){
			trouve=0;
			if( disk->inode[i].typeFile==3){
				disk->inode[i]=mycreat("0\0");
				trouve=1;
			}
		}
	}
	if(trouve==0){
		printf("Error: %s is not a link. Use myrm instead\n",name);
	}
	if(trouve==-1){
		printf("Error: %s cannot be find\n",name);
	}


}
INODE mymkdir(DISK *disk,char *name,int inodefather,int inodej ){
		INODE inode;
		int k=0;
		char buffer[50];
		// Initialization
		inode.typeFile = 2;
		inode.lenghtFile = 0;
		inode.blockNumber = init_list();
		for(k=0;k<20;k++){
			inode.name[k]=name[k];
		}
		inode.state = 1;
		inode.permission[0]='r';
		inode.permission[1]='w';
		inode.permission[2]='x';

		//Write in block
		sprintf(buffer,".:%d..:%d",inodej,inodefather);

		mywrite(disk,&inode,buffer);
		printf("folder create with succes\n");
		
		for(k=0;k<13;k++){
			inode.modificationDate[k]=0;
		}
		
		return inode;
	}

typedef struct{
	char ** tab;
	int taille;
}stringTab;

stringTab stringToTab(char * s,char * sep){
	char *s2 = strdup(s);
	char *token = strtok(s2,sep);

	int imax =0;
    while(token!=NULL) {
        token = strtok(NULL,sep);
        imax++;
    }
    int i;
    char **retour = malloc(imax*sizeof(char * ));
    for(i=0;i<imax;i++){
		retour[i]=malloc(sizeof(char *));
	}

    s2 = strdup(s);
	token = strtok(s2,sep);
	i =0;
    while(token!=NULL) {
		strcpy(retour[i],token);
		token = strtok(NULL,sep);
        i++;
    }
    stringTab retour2;
    retour2.tab=retour;
    retour2.taille=imax;
	return retour2;
}

int pathToInode(DISK disk, char *path,int inode){
    stringTab stringpath = stringToTab(path,"/");
    stringTab fic;
    int i,j;
    printf("%s\n",path);
    //printf("%d\n",stringpath.taille);

    for(i=0;i<stringpath.taille;i++){
		//printf("%s\n",stringpath.tab[i]);

		INODE lecture= myopen(disk,stringpath.tab[i],"r");
		fic = stringToTab(myread(disk,lecture),":\n");
		//printf("%d\n",fic.taille);
		myclose(&lecture);
		for(j=0;j<fic.taille;j++){
			printf("%s\n",fic.tab[j]);
			if(strcmp(fic.tab[j],stringpath.tab[i])==0){
				break;
			}
		}		
		if(fic.taille==0 || strcmp(fic.tab[j],stringpath.tab[i])!=0){
			printf("error incorrect path\n");
			return -1;
		}
	}
	for(i=0;i<NBINODE;i++){
		if(strcmp(stringpath.tab[stringpath.taille],disk.inode[i].name)==0){
			return i;
		}
	}
	return -1;
}


void myrmdir(DISK *disk, char *name){
	int i=0;
	for(i=0;i<NBINODE;i++){
		if(name == disk->inode[i].name){
			printf("size : %d \n",disk->inode[i].lenghtFile);
			if(disk->inode[i].typeFile == 2){
				if(disk->inode[i].lenghtFile == 1){
					myrm(disk,name);
				}
				else {
					printf("the folder isn't empty\n");
				}
			}
			else{
				printf("the file isn't a folder\n");
			}
		}
	}

}
