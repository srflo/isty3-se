#ifndef PRIMITIVES_H
#define PRIMITIVES_H
#include "linked_lists.h"
#include "fms.h"

INODE mycreat(char *name);
INODE myopen(DISK disk, char *name, char *mode);
void myclose(INODE *inode);
char* myread(DISK disk, INODE inode);
void mywrite(DISK *disk, INODE *inode, char *buffer);
void myrm(DISK *disk, char *name);
INODE mymkdir(DISK *disk,char *name,int inodefather,int inodej);
void myrmdir(DISK *disk,char *name);
void mylink(DISK *disk, char *name, char *name2);
void myunlink(DISK *disk,char *name);
int pathToInode(DISK disk, char *path,int inode);

#endif
